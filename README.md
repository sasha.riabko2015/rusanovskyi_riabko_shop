# Rusanovskyi Riabko - Online Glasses Shop

## brief description

Glasses store for men and women built on MERN stack

## start development

**to run watchers concurrently on FE and BE**

```
npm run dev
```

**to run sonar qube lint**

```
npm run sonar
```

## mongo db schema

|  `User`  |  types  |     |     |     | `Review` | types  |
| :------: | :-----: | :-: | :-: | :-: | :------: | :----: |
|   name   | String  |     |     |     |   name   | String |
|  email   | String  |     |     |     |  rating  | Number |
| password | String  |     |     |     | comment  | String |
| isAdmin  | Boolean |     |     |

|    `Product`    |  types   |     |     |     |     `Order`     |                   types                   |
| :-------------: | :------: | :-: | :-: | :-: | :-------------: | :---------------------------------------: |
|      user       |  `User`  |     |     |     |      user       |                  `User`                   |
|      name       |  String  |     |     |     | shippingAddress |  { adress, city, country, postalOffice}   |
|      name       |  String  |     |     |     |  paymentResult  | { id, status, update_time, email_address} |
|      image      |  String  |     |     |     |   orderItems    | Array<`Product`, name, brand, price, qty> |
|      brand      |  String  |     |     |     |  paymentMethod  |                  String                   |
|    category     |  String  |     |     |     |    taxPrice     |                  Number                   |
|     gender      |  String  |     |     |     |  shippingPrice  |                  Number                   |
|     reviews     | `Review` |     |     |     |   totalPrice    |                  Number                   |
|     rating      |  Number  |     |     |     |     isPaid      |                  Boolean                  |
|   numReviews    |  Number  |     |     |     |   isDelivered   |                  Boolean                  |
| frameForGlasses |  String  |     |     |     |     paidAt      |                   Date                    |
|  frameMaterial  |  String  |     |     |     |   deliveredAt   |                   Date                    |
|    lensColor    |  String  |     |     |     |
|  lensMaterial   |  String  |     |     |     |
|  polarization   | Boolean  |     |     |     |
|    mirrored     | Boolean  |     |     |     |
|    gradient     | Boolean  |     |     |     |
|      sale       | Boolean  |     |     |     |
|     percent     |  Number  |
|      price      |  Number  |
|  countInStock   |  Number  |

## Contributors: _Rusanovskyi D._ _Riabko O._
