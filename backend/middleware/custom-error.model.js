export class CustomError {
  message;
  status;
  additionalInfo;

  constructor(message, status = 500, additionalInfo = {}) {
    this.message = message;
    this.status = status === 200 ? 500 : status;
    this.additionalInfo = additionalInfo;
  }
}
