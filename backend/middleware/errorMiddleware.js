import { CustomError } from './custom-error.model.js';

const notFound = (req, _res, next) => {
  const error = new CustomError(`Not Found`, 404, {
    id: 'not_found1',
    title: 'Page Not Found',
    detail: `Not Found - ${req.originalUrl}`,
  });
  next(error);
};

const errorHandler = (err, _req, res, _next) => {
  let customError = err;

  if (!err) {
    customError = new CustomError('Unexpected Error Occured!');
  }

  // we are not using the next function to prvent from triggering
  // the default error-handler. However, make sure you are sending a
  // response to client to prevent memory leaks in case you decide to
  // NOT use, like in this example, the NextFunction .i.e., next(new Error())
  res.status(customError.status).send(customError);
};

export { notFound, errorHandler };
