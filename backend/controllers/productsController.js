import asyncHandler from 'express-async-handler';
import { CustomError } from '../middleware/custom-error.model.js';
import Product from '../models/productModel.js';

// @desc Fetch all products
// @route Get /api/products
// @access Public
const getProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({});
  if (products) {
    res.json(products);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found7',
      title: 'Products Not Found',
      detail: 'Товары не найдены',
    });
  }
});

// @desc Fetch all products for women
// @route Get /api/products/women
// @access Public
const getProductsByFemale = asyncHandler(async (req, res) => {
  const products = await Product.find({ gender: 'female' });
  if (products) {
    res.json(products);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found8',
      title: 'Female Products Not Found',
      detail: 'Женские товары не найдены',
    });
  }
});

// @desc Fetch 9 products for women
// @route Get /api/products/women
// @access Public
const getProductsByFemaleLimit = asyncHandler(async (req, res) => {
  const products = await Product.find({ gender: 'female' }).limit(8);
  if (products) {
    res.json(products);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found9',
      title: 'Female Products Not Found',
      detail: 'Женские товары не найдены',
    });
  }
});

// @desc Fetch all products for men
// @route Get /api/products/men
// @access Public
const getProductsByMale = asyncHandler(async (req, res) => {
  const products = await Product.find({ gender: 'male' });
  if (products) {
    res.json(products);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_foun10',
      title: 'Male Products Not Found',
      detail: 'Мужские товары не найдены',
    });
  }
});

// @desc Fetch limit products for men
// @route Get /api/products/men
// @access Public
const getProductsByMaleLimit = asyncHandler(async (req, res) => {
  const products = await Product.find({ gender: 'male' }).limit(8);
  if (products) {
    res.json(products);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found11',
      title: 'Male Products Not Found',
      detail: 'Мужские товары не найдены',
    });
  }
});

// @desc Fetch single product
// @route Get /api/products/:id
// @access Public
const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);
  if (product) {
    res.json(product);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found12',
      title: 'Product Not Found',
      detail: 'Товар не найден',
    });
  }
});

// @desc Fetch all sale products
// @route Get /api/products/sale
// @access Public
const getProductBySale = asyncHandler(async (req, res) => {
  const product = await Product.find({ sale: true });
  if (product) {
    res.json(product);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found13',
      title: 'Product With Sale Not Found',
      detail: 'Товар со скидкой не найден',
    });
  }
});

const getTopProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({}).sort({ rating: -1 }).limit(10);
  if (products) {
    res.json(products);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found14',
      title: 'Top Products Not Found',
      detail: 'Лучшие Товары не найдены',
    });
  }
});

export {
  getProducts,
  getProductById,
  getProductsByFemale,
  getProductsByMale,
  getProductsByFemaleLimit,
  getProductsByMaleLimit,
  getProductBySale,
  getTopProducts,
};
