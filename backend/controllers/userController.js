import asyncHandler from 'express-async-handler';
import generateToken from '../utils/generateToken.js';
import User from '../models/userModel.js';
import { CustomError } from '../middleware/custom-error.model.js';

// @desc    Auth user & get token
// @route   POST /api/users/login
// @access  Public
const authUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });

  if (user && (await user.matchPassword(password))) {
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      token: generateToken(user._id),
    });
  } else {
    throw new CustomError('Bad Authorization', 401, {
      id: 'bad_authorization1',
      title: 'Invalid Email Or Password',
      detail: 'неправильный имейл или пароль',
    });
  }
});

// @desc    Register a new user
// @route   POST /api/users
// @access  Public
const registerUser = asyncHandler(async (req, res) => {
  const { name, email, password } = req.body;

  const userExists = await User.findOne({ email });

  if (userExists) {
    throw new CustomError('Bad Registration', 400, {
      id: 'bad_request2',
      title: 'User Already Exists',
      detail: 'такой юзер уже существует',
    });
  }

  const user = await User.create({
    name,
    email,
    password,
  });

  if (user) {
    res.status(201).json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      token: generateToken(user._id),
    });
  } else {
    throw new CustomError('Bad Registration', 400, {
      id: 'bad_request3',
      title: 'Invalid User Data',
      detail: 'неправильно заполнены поля для регистрации',
    });
  }
});

// @desc    Update user profile
// @route   PUT /api/users/profile
// @access  Private
const updateUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    if (req.body.password) {
      user.password = req.body.password;
    }

    const updatedUser = await user.save();

    if (updatedUser) {
      res.json({
        _id: updatedUser._id,
        name: updatedUser.name,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        token: generateToken(updatedUser._id),
      });
    } else {
      throw new CustomError('Forbidden Creds', 403, {
        id: 'forbidden_request2',
        title: 'Forbidden Creds Update',
        detail: 'неудачная попытка обновления пароля или логина',
      });
    }
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found15',
      title: 'User Not Found',
      detail: 'пользователь не найден',
    });
  }
});

// @desc    Get user profile
// @route   GET /api/users/profile
// @access  Private
const getUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    });
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found16',
      title: 'User Not Found',
      detail: 'пользователь не найден',
    });
  }
});

// @desc    Get user profile
// @route   GET /api/users
// @access  Private
const getUsers = asyncHandler(async (req, res) => {
  const users = await User.find({});
  if (users) {
    res.json(users);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found17',
      title: 'Users Not Found',
      detail: 'пользователи не найден',
    });
  }
});

export { authUser, registerUser, getUserProfile, updateUserProfile, getUsers };
