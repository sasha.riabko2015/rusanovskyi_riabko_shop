import asyncHandler from 'express-async-handler';
import { CustomError } from '../middleware/custom-error.model.js';
import Order from '../models/orderModel.js';

// @desc    Create new order
// @route   POST /api/orders
// @access  Private
const addOrderItems = asyncHandler(async (req, res) => {
  const {
    orderItems,
    shippingAddress,
    paymentMethod,
    itemsPrice,
    taxPrice,
    shippingPrice,
    totalPrice,
  } = req.body;

  if (orderItems?.length === 0) {
    throw new CustomError('Invalid Request', 400, {
      id: 'bad_request1',
      title: 'No Order Positions',
      detail: 'Нет позиций заказа',
    });
  } else {
    const order = new Order({
      orderItems,
      user: req.user._id,
      shippingAddress,
      paymentMethod,
      itemsPrice,
      taxPrice,
      shippingPrice,
      totalPrice,
    });

    const createdOrder = await order.save();

    if (createdOrder) {
      res.status(201).json(createdOrder);
    } else {
      throw new CustomError('Forbidden Request', 403, {
        id: 'forbidden_request1',
        title: "Couldn't create an order",
        detail: 'Не получилось создать заказ',
      });
    }
  }
});

// @desc    Get order by ID
// @route   GET /api/orders/:id
// @access  Private
const getOrderById = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id).populate(
    'user',
    'name email'
  );

  if (order) {
    res.json(order);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found2',
      title: 'Order Not Found',
      detail: 'Заказ не найден',
    });
  }
});

// @desc    Update order to paid
// @route   GET /api/orders/:id/pay
// @access  Private
const updateOrderToPaid = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id);

  if (order) {
    order.isPaid = true;
    order.paidAt = Date.now();
    order.paymentResult = {
      id: req.body.id,
      status: req.body.status,
      update_time: req.body.update_time,
      email_address: req.body.payer.email_address,
    };

    const updatedOrder = await order.save();

    res.json(updatedOrder);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found3',
      title: 'Order Not Found',
      detail: 'Заказ не найден',
    });
  }
});

// @desc    Update order to delivered
// @route   GET /api/orders/:id/deliver
// @access  Private/Admin
const updateOrderToDelivered = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id);

  if (order) {
    order.isDelivered = true;
    order.deliveredAt = Date.now();

    const updatedOrder = await order.save();

    res.json(updatedOrder);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found4',
      title: 'Order Not Found',
      detail: 'Заказ не найден',
    });
  }
});

// @desc    Get logged in user orders
// @route   GET /api/orders/myorders
// @access  Private
const getMyOrders = asyncHandler(async (req, res) => {
  const orders = await Order.find({ user: req.user._id });
  if (orders) {
    res.json(orders);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found5',
      title: 'User Orders Not Found',
      detail: 'Заказы пользователя не найдены',
    });
  }
});

// @desc    Get all orders
// @route   GET /api/orders
// @access  Private/Admin
const getOrders = asyncHandler(async (req, res) => {
  const orders = await Order.find({}).populate('user', 'id name');
  if (orders) {
    res.json(orders);
  } else {
    throw new CustomError('Not Found', 404, {
      id: 'not_found6',
      title: 'Orders Not Found',
      detail: 'Заказы не найдены',
    });
  }
});

export {
  addOrderItems,
  getOrderById,
  updateOrderToPaid,
  updateOrderToDelivered,
  getMyOrders,
  getOrders,
};
