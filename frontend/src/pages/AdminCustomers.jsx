import React from "react";
import Customers from "../components/Admin/Customers/Customers";

const AdminCustomers = () => {
  return (
    <div className="container">
      <Customers/>
    </div>
  );
};

export default AdminCustomers;
