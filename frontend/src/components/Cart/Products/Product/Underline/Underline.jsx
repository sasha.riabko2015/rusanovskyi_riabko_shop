import React from "react";
import styles from "../../../Cart.module.scss";

const Underline = () => <div className={styles.underline}></div>;

export default Underline;
